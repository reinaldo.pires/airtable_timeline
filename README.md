# Airtable Timeline app

## Introduction

This is a simple timeline app, loading a Airtable base using REST API.
As a long time cross platform development defender, and experiencing again in my last role the pain points of duplicate efforts involved in separate code bases for diffent platforms, I decided to create a Android/iOS app using Flutter. 

It has been tested in Android/iOS simulators and on an Android 8 device.

Project code at Gitlab:
https://gitlab.com/reinaldo.pires/airtable_timeline

Here is a short video with the app in action:
https://www.dropbox.com/s/pbzuqxbozxfgyey/airtable_timeline.mp4?dl=0

Please get the debug APK from this link:
https://www.dropbox.com/s/9q9m99zyn6iq257/timeline_debug_2019-06-24.apk?dl=1

Get iOS build from TestFlight:
https://testflight.apple.com/join/D9hOTQNi

Note: I plan to make an iOS build available at TestFlight, I will let you know when I have it done.

## Project structure
- `/lib`: folder with the app's code, all the other folders/files are related to Flutter/VSCode project structure.
- `/lib/timeline_screen.dart`: main screen code, including the timeline grid widget logic;
- `/lib/widgets/timeline_task_widget.dart`: class to render and interact with the task bar in the grid; 
- `/lib/widgets/models`: model classes for the timeline and for tasks;
- `/lib/widgets/services/airtable_service.dart`: provides methods for interacting with Airtable API;

## Features available
 - Task list is loaded via REST API from the following Airtable Base: <br/> https://airtable.com/invite/l?inviteId=invfgjklvmy3jRecD&inviteToken=704e35ab19c6ba2ce117a99be826e267bf07ab39e9c349a21d9c41a3269ac302
- Move a task by dragging its center;
- Edit start/end date by dragging left/right edge of the task;
- When moving/altering a task, conflicting tasks will be rearranged; 
- Zooming in and out using pinch gesture.

## How to build 

1. Install Flutter SDK/Tools: https://flutter.dev/docs/get-started/install
2. Android: At the command line, from this project root folder, run:
    *  `flutter build apk --debug` (https://flutter.dev/docs/deployment/android#build-an-apk)
3. iOS: https://flutter.dev/docs/deployment/ios


## Questions and Answers

1. How long you spent on the assignment. <br/>
A: 2 days learning Flutter as it was in my "to learn" list for some time and this was a good opportunity.<br/>
2 days creating the app, including researching/resolving doubs as a first time Flutter user.<br/>
Note: Flutter hot reload is really amazing! :)

2. What you like about your implementation.<br/>
A: It is simple, yet it provides a good base for building a more timeline and gantt chart block for mobile. The same exact code works on Android and iOS. And possibly can create generate desktop versions. 

3. What you would change if you were going to do it again.<br/>
A: I would research other ways of constructing the grid.

4. How you made your design decisions. For example, if you looked at other timelines for inspiration, please note that.<br/>
A: I took a look at how the Airtable Timeline block works. Besides that, I have used a lot of different tools for tracking projects, hence I had a good idea on what was needed in terms of usability.

5. How you would test this if you had more time.<br/>
A: I would create test cases in these order, following Flutter docs recommendation:<br/>
    - https://flutter.dev/docs/testing 
    1. Unit tests for the model classes;
    2. Widget tests for checking the widgets behaviours;
    3. Integration tests using `flutter_driver`: <br/> https://flutter.dev/docs/cookbook/testing/integration/introduction 

    Also, I would have other persons test the usability on real devices to get feedback.
I did it and found and fixed some critical issues regarding the drag, which worked fine on emulator but was very bad on the real device.


## Features and enhancements for next versions

* Address a few TODOs present in the code, including extracting the timeline grid widget in a separate widget file/class.
* Improve layouting logic for cases with big tasks list.
  First test/profile on real devices these cases and optimize the solution only if needed;
* When dragging a task to the far screen left or right sides, scroll the grid for a better user experience
* Improving on previous point, If reaching the end of the grid, generate on demand new columns(days) for dropping the task;
* Improve the zooming functionaly, as the gesture is not workng very smoth and there are some small issues with the list when zoomed out.
* Task Edition: Tap the task to Open an editing sheet/page;
* Send task changes back to Airtable service;
* Options to configure:
    - Aritable base url/api key to use;
    - Select fields to use as title, start/end dates;
    - Select color per task or choose groups;
* Gantt like task dependencies:
    - link tasks visually in the grid and/or via task editing page;
    - moving a task linked to others may move the others as well.   
* If there is an API to load bases on demand(instead of current per base API Key):
    - List available bases from the user to view as Timeline;
    - Just like web blocks, options to define each column to be used as the importante fields.   
* Offline mode:  investigate if it would make sense to enable users to work offline with a base.<br/>
Apparently this offline editing is not supported on Airtable mobile Apps, at least on Android side.

