import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/rendering.dart';
import 'package:airtable_timeline/constants.dart';
import 'package:airtable_timeline/widgets/timeline_task_widget.dart';
import 'package:airtable_timeline/models/timeline_model.dart';
import 'package:airtable_timeline/services/airtable_service.dart' as airtable;
import 'package:airtable_timeline/models/task_model.dart';

class TimelinePage extends StatefulWidget {
  TimelinePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TimelinePageState createState() => _TimelinePageState();
}

class _TimelinePageState extends State<TimelinePage> {
  Timeline timeline;
  double scale = 1.0;

  @override
  initState() {
    super.initState();

    loadTasks();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called

    final Color color = Colors.grey[200];

    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: _buildListView(color));
  }

  void onScaleUpdate(ScaleUpdateDetails details){
      setState(() {
         scale = details.scale;
       });
  }

  Widget _buildListView( Color color) {
    if (timeline == null) {
      return new Center(
        child: new CircularProgressIndicator(),
      );
    } else {

      var chartViewWidth = (timeline.lenght() + (GRID_DAYS_MARGIN * 2) ) * GRID_COL_WIDTH; 

      return Scrollbar(
            child: ListView(
              physics: new ClampingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                GestureDetector(onScaleUpdate: onScaleUpdate,
              child: 
            Transform.scale(
                scale: scale,
                alignment: Alignment.topLeft,
                child: Stack(children: <Widget>[
                  buildGrid(chartViewWidth ),
                  buildHeader(chartViewWidth, color),
                ]))),
          ]));
    }
  }

  Widget buildHeader(double chartViewWidth, Color color) {
    List<Widget> headerItems = new List();

    DateTime tempDate = timeline.startDate;
    DateFormat dateFormat = DateFormat("MMM d");

    for (int i = 0; i < timeline.lenght(); i++) {
      headerItems.add(Container(
        width: GRID_COL_WIDTH, //chartViewWidth / viewRangeToFitScreen,
        child: new Text(
          dateFormat.format(tempDate),
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 10.0, fontWeight: FontWeight.bold),
        ),
      ));
      tempDate = tempDate.add(Duration(days: 1));
    }

    return Container(
      height: 25.0,
      color: color,
      child: Row(
        children: headerItems,
      ),
    );
  }

  Widget buildGrid(double chartViewWidth) {
    List<Widget> gridColumns = new List();

    for (int i = 0; i <= timeline.lenght(); i++) {
      gridColumns.add(Container(
        decoration: BoxDecoration(
            border: Border(
                right:
                    BorderSide(color: Colors.grey.withAlpha(100), width: 1.0))),
        width: GRID_COL_WIDTH,
        height: GRID_HEIGHT, //chartViewWidth / viewRangeToFitScreen,
      ));
    }

    List<Widget> timelineTasks = <Widget>[Row(children: gridColumns)];
    var allTasks = TimelineTask.listFromTaskMap(timeline.tasks, _updateTask);
    if (allTasks != null) timelineTasks.addAll(allTasks);

    return SingleChildScrollView(child: Stack(children: timelineTasks));
  }

  void buildAllTasks(List<Widget> widgets) {
    if (timeline == null) return;

    timeline.tasks.forEach((key, task) {
      // TimelineTask()
    });
  }

  void loadTasks() {
    airtable.getAllTasks().then((tasks) {
      setState(() {
        timeline = Timeline(tasks);
        timeline.sortTasksByStart();
        timeline.initTasksPosition();
      });
    }).catchError((error) {
      print('Error trying to get  : $error');
      _showError(error.toString());
    });
  }

  void _updateTask(Task task) {
    // TODO: call service to send updated task info to server

    setState(() {
      timeline.resolveIntersections(task);
    });
  }

  _showError(String msg) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () => Navigator.pop(context),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Error"),
      content: Text("Unable to process the request: \n" + msg),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
