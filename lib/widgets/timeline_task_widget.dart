import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:airtable_timeline/models/task_model.dart';
import 'package:airtable_timeline/constants.dart' as cons;

class TimelineTask extends StatefulWidget {
  TimelineTask({Key key, this.task, this.updateTask }) : super(key: key);

  final Task task;
  final Function updateTask;
  final GlobalKey _key = GlobalKey();

  @override
  _TimelineTaskState createState() => _TimelineTaskState();

  static List<TimelineTask> listFromTaskMap(Map<String, Task> tasksMap , Function _updateTask){
      List<TimelineTask> timelineTasks = [];
      tasksMap.forEach((k, task){
          timelineTasks.add(TimelineTask(task: task , updateTask: _updateTask));
      });

      return timelineTasks;
  }
}

class _TimelineTaskState extends State<TimelineTask> {
  bool isUpdating = false;

  void _updateSize(Offset offSet,  {bool isDragEnd : false, bool isStartDate: true}){
    if(!isDragEnd)
    {
      setState(() {
        isUpdating = true;
        // check if dealing with start or end date
        var pos =  isStartDate ? widget.task.startPos : widget.task.endPos;
        pos = (pos + (offSet.dx * cons.DRAG_THRESHOLD * cons.GRID_COL_WIDTH));
        isStartDate ? widget.task.startPos = pos : widget.task.endPos = pos;
      });
    } else {
      setState(() {
        isUpdating = false;
        // do position snap to the day:
        // - If startDate, snap to next left side.
        // - If endDate, do snap to next right date.
        var oldPos =  isStartDate ? widget.task.startPos : widget.task.endPos;
        var gridPos = ( (isStartDate ? 0 : 1) + (oldPos / cons.GRID_COL_WIDTH).floor());
        var oldGridPos = isStartDate ? widget.task.startGridPos : widget.task.endGridPos;
        var pos = gridPos * cons.GRID_COL_WIDTH;
        isStartDate ? widget.task.startPos = pos : widget.task.endPos = pos;

        _updateDate( gridPos - oldGridPos, isStartDate);
        widget.updateTask(widget.task);

      });
    }
  }

  void _updateDate(int dx, bool isStartDate ){
    // TODO this should be moved to task_model
    DateTime date = isStartDate ? widget.task.startDate : widget.task.endDate;
    date = date.add(Duration(days: dx));
    
    if(isStartDate){
      widget.task.startGridPos += dx;
      widget.task.startDate = date;
    } else {
      widget.task.endGridPos += dx;
      widget.task.endDate = date;
    }

  }

  void _updatePos(Offset offSet , {bool isDragEnd: false}){
    if(!isDragEnd)
    {
      setState(() {
        isUpdating = true;
        // check if dealing with start or end date
        var pos = (widget.task.startPos + (offSet.dx * cons.DRAG_THRESHOLD * cons.GRID_COL_WIDTH));
        var posOffset = widget.task.startPos - pos;
        widget.task.startPos = pos;
        widget.task.endPos -= posOffset;
      });
    } else {
      setState(() {
        isUpdating = false;
        var width = widget.task.endPos - widget.task.startPos;
        widget.task.startPos = (widget.task.startPos / cons.GRID_COL_WIDTH).floor() * cons.GRID_COL_WIDTH;
        widget.task.endPos = widget.task.startPos + width;

        // TODO this should be moved/treated at timeline_model
        var gridPos = (widget.task.startPos / cons.GRID_COL_WIDTH).floor();
        var oldGridPos = widget.task.startGridPos;
        _updateDate( gridPos - oldGridPos, true);
        _updateDate( gridPos - oldGridPos, false);

        widget.updateTask(widget.task);
      });
    }

  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
        left: widget.task.startPos,
        top: (40.0 * widget.task.line),
        width: widget.task.endPos - widget.task.startPos,
        height: cons.BAR_HEIGHT,
        // child: Tooltip(
        //     message:  "Start - End: ${dateFormat.format(widget.task.startDate)} - ${dateFormat.format(widget.task.endDate)}",
            child: Container(
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(4) , color: Colors.pink[400].withAlpha(isUpdating ? 128 : 255 )),

              
              child: Stack(
                
                children: <Widget>[
                      GestureDetector(
                          onHorizontalDragUpdate: (dragUpdateDetails) {
                            _updatePos(dragUpdateDetails.delta);
                          },
                          onHorizontalDragEnd: (dragEndDetails) {
                            _updatePos(null, isDragEnd: true);
                          },
                          child: Align( key: widget._key ,
                                    alignment: Alignment.center,
                                    child: Text(widget.task.title, 
                                                textAlign: TextAlign.center,
                                                style: TextStyle(color: Colors.white, fontSize: 10, fontWeight: FontWeight.bold),   
                                            )
                                  )
                      ),
                      GestureDetector(
                          onHorizontalDragUpdate: (dragUpdateDetails) {
                            _updateSize(dragUpdateDetails.delta);
                          }, 

                          onHorizontalDragEnd: (dragEndDetails) {
                            _updateSize(null, isDragEnd: true);
                          }
                          ,
                          child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      color: Colors.black.withAlpha(0),
                                      width: 15,
                          )
                          )
                      ),  
                       GestureDetector(
                          onHorizontalDragUpdate: (dragUpdateDetails) {
                            _updateSize(dragUpdateDetails.delta, isStartDate: false);
                          }, 

                          onHorizontalDragEnd: (dragEndDetails) {
                            _updateSize(null, isDragEnd: true , isStartDate: false);
                          }
                          ,
                          child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Container(
                                      color: Colors.black.withAlpha(0),
                                      width: 15,
                          )
                          )
                      ),
                ],
              ), 
            )
            //)
            );
  }

}