import 'package:flutter/material.dart';
import 'package:airtable_timeline/timeline_screen.dart';

void main() => runApp(AirtableApp());

class AirtableApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Airtable',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: TimelinePage(title: 'Airtable Timeline'),
    );
  }


}
