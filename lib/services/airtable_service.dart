import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:airtable_timeline/models/task_model.dart';
import 'package:airtable_timeline/constants.dart';
import 'dart:collection';

const HEADERS = {
                  HttpHeaders.contentTypeHeader: 'application/json',
                  HttpHeaders.authorizationHeader : 'Bearer $API_KEY'
                };

Future<HashMap<String, Task>> getAllTasks() async {
  final response = await http.get(URL_TASKS_GET , 
                  headers: HEADERS).timeout(Duration(seconds: 10));

  return Task.hashMapFromJson(json.decode(response.body) );
}

// Future<http.Response> upatedTask(Task task) async{
//   final response = await http.post('$url',
//       headers: HEADERS,
//       body: dateChangeToJson(Task)
//   );
//   return response;
// }