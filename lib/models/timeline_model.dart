import 'dart:collection';
import 'package:airtable_timeline/models/task_model.dart';
import 'package:airtable_timeline/constants.dart' as cons;


class Timeline {
  Map<String, Task> tasks;
  DateTime startDate;
  DateTime endDate;

  static const MAX_LINES = 6;

  Timeline(this.tasks ); 

  void initTasksPosition(){
    // first we need to calculate all positions
    tasks.forEach((k,task) {  
      task.setPosition(startDate);
      findFreeSpace(task);
    });
  }

  void sortTasksByStart(){
      SplayTreeMap<String, Task> sorted = new SplayTreeMap.of(tasks, ( a,  b) => tasks[a].startDate.isBefore(tasks[b].startDate) ? -1 : 1 );
      tasks = sorted;

      setStartAndEndDate();
  }

  int lenght(){
    if(startDate == null || endDate == null)
      return 0;

    return endDate.difference(startDate).inDays; 
  }

  void setStartAndEndDate(){
    startDate = tasks.values.elementAt(0).startDate;
    endDate = tasks.values.elementAt(0).endDate;
    tasks.forEach((k,v){
      endDate = endDate.isBefore(v.endDate) ? v.endDate : endDate;
    });

    // keep 3 days before and after to allow moving tasks to these ranges
    startDate = startDate.add(Duration(days: -cons.GRID_DAYS_MARGIN));
    endDate   = endDate.add(Duration(days: cons.GRID_DAYS_MARGIN));
  }

  // helper method for debug purposes
  void printTasks()
  {
    tasks.forEach((k,v){
      String title = v.title;
      String start = v.startDate.toIso8601String();
      print("[$k] $start $title");
    });
  }  

  void addTask(Task newTask){
    tasks[newTask.id] = newTask;
  }
  
  // will check if there are any other task intersecting and
  // move the intersecting tasks to free slots
  void resolveIntersections(Task targetTask){
    List<Task> intersections = _tasksIntersecting(targetTask);

    for(Task task in intersections ){
      findFreeSpace(task);
    }
  }

  List<Task> _tasksIntersecting(Task targetTask){
    List<Task> result = [];

    tasks.forEach((k,task){
      // make sure to not include the same task and neither tasks froms
      // different lines
      if(task.id != targetTask.id && task.line == targetTask.line){
        if(targetTask.taskOverlap(task)){
          result.add(task);
        }
      }
    });

    return result;
  }

  // loop over lines to check which one has a free slot for this task
  void findFreeSpace(Task targetTask){
    int line = 0; 
    bool spaceFound = false;
     
    while(line < MAX_LINES && !spaceFound){
      line++;
      spaceFound = _freeSpaceInLine(line , targetTask);
    }
    targetTask.line = line; 
  }

  bool _freeSpaceInLine(int line ,  Task targetTask){
      for(Task task in tasks.values){
        if(task.id != targetTask.id && task.line == line){
            // if finds a overlapping in the same line, 
            //  break this loop to move to the next one
            if(targetTask.taskOverlap(task)){
              return false;
            }
          }
      } 

      return true;
  }

}