import 'dart:collection';
import 'package:airtable_timeline/constants.dart' as cons;

class Task {
  String id;
  String title;
  DateTime startDate;
  DateTime endDate;
  // line to be rendered. When there is tasks intersection, this will be updated 
  // to reflect a line where it will be diplayed correctly
  int line = 0; 
  double startPos = -1;
  double endPos =  -1;
  // usefull to keep track positioning related to the start of the grid
  int startGridPos = 0;
  int endGridPos = 0;

  // NOTE: just for the programming test, this class is considering only the fields 
  // defined in the following base:
  // https://api.airtable.com/v0/appUxCMWwZeLmPA3y/Tasks?maxRecords=3&view=All%20tasks

  Task({
      this.id,
      this.title,
      this.startDate,
      this.endDate,
  });

  factory Task.fromJson(Map<String, dynamic> parsedJson){
    return Task(
      id: parsedJson['id'],
      title: parsedJson['fields']['Task'],
      startDate : DateTime.tryParse(parsedJson['fields']['Start date']),
      endDate : DateTime.tryParse(parsedJson['fields']['End date']),
    );
  }

  static  Map<String, Task> hashMapFromJson(Map<String, dynamic> parsedJson){
      Map<String, Task> tasks = HashMap<String, Task>();

      if(parsedJson != null){
        for(var record in parsedJson['records']){
          Task task = Task.fromJson(record);
          tasks[task.id] = task;
        }
      }

      return tasks;
  }

  // the positions must be calculated relatively to the 
  // date used for the start of the grid
  void setPosition(DateTime gridStartDate){
    startGridPos = startDate.difference(gridStartDate).inDays;
    endGridPos = 1 + endDate.difference(gridStartDate).inDays;

    startPos =  startGridPos * cons.GRID_COL_WIDTH;
    endPos = startPos + ((endDate.difference(startDate).inDays + 1) * cons.GRID_COL_WIDTH);
  }

  bool taskOverlap(Task anotherTask){
    bool result1 = _datesOverlap(anotherTask.startDate , startDate, endDate);
    bool result2 = _datesOverlap(anotherTask.endDate , startDate, endDate);
    bool result3 = _datesOverlap(startDate, anotherTask.startDate, anotherTask.endDate);
    bool result4 = _datesOverlap(endDate, anotherTask.startDate, anotherTask.endDate);

    return    result1 || result2 || result3 || result4;
  }

  bool _datesOverlap(DateTime toCheck , DateTime start, DateTime end){
    bool result1 = (toCheck.isAfter(start) || toCheck.isAtSameMomentAs(start)) ;
    bool result2 = (toCheck.isBefore(end)  || toCheck.isAtSameMomentAs(end));
    
    return result1 && result2;
  }

}